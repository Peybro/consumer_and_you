import * as React from "react";

import { Helmet } from "react-helmet";
import styled from "styled-components";

import Consumer from "../components/Consumer";
import GlobalLayout from "./GlobalLayout";

//styles
const IntroPage = styled.div`
  height: 100vh;
  width: 100vw;
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  position: relative;

  @media screen and (max-width: 320px) {
    overflow-y: scroll;
    align-items: initial;
  }
`;
const IntroContent = styled.div`
  z-index: 10;

  @media screen and (max-width: 320px) {
    margin-top: 20px;
  }
`;
const IntroBackground = styled.div`
  font-style: italic;
  position: absolute;
  word-break: break-all;
`;

// markup
const Layout = ({ children }) => {
  return (
    <GlobalLayout>
      <IntroPage>
        <Helmet>
          <title>Overview</title>
          <meta name="description" content="Do this or that." />
        </Helmet>
        <IntroBackground>
          <Consumer amount={4000} />
        </IntroBackground>

        <IntroContent>{children}</IntroContent>
      </IntroPage>
    </GlobalLayout>
  );
};

export default Layout;
