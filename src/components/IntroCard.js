import React from "react";

import { Link } from "gatsby";
import styled from "styled-components";

const Card = styled(Link)`
  display: flex;
  max-width: 400px;
  max-height: 400px;
  overflow: hidden;
  position: relative;
  justify-content: center;
  align-items: center;
  background: #fff;

  @media screen and (min-width: 100px) {
    width: 80vw;
    height: 80vw;
  }

  @media screen and (min-width: 720px) {
    width: 40vw;
    height: 40vw;
  }
`;

const CardImg = styled.img`
  width: 80%;
  position: absolute;
  filter: grayscale(1) contrast(0.5);
`;

const CardBody = styled.div`
  position: absolute;
  color: yellow;
  font-style: italic;

  @media screen and (min-width: 100px) {
    font-size: 3rem;
  }

  @media screen and (min-width: 720px) {
    font-size: 4rem;
  }
`;

export default function CategoryCard({ title, to, img }) {
  return (
    <Card to={to}>
      <CardImg alt={img.alt} src={img.src} />
      <CardBody>
        <p>{title}</p>
        <p>{title}</p>
        <p>{title}</p>
        <p>{title}</p>
      </CardBody>
    </Card>
  );
}
