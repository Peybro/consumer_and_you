import React, { useEffect, useState } from "react";

import styled from "styled-components";

const Container = styled.div`
  max-width: 777px;
  max-height: 436px;

  & iframe,
  & object,
  & embed {
    width: 100%;
    height: 100%;
  }
`;

export default function Video({ title, src }) {
  const [style, setStyle] = useState({
    width: window.innerWidth * 0.8,
    height: (window.innerWidth * 0.8) / (777 / 436),
  });

  const handleResize = () => {
    setStyle({
      width: window.innerWidth * 0.8,
      height: (window.innerWidth * 0.8) / (777 / 436),
    });
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return (
    <Container style={style}>
      <iframe
        title={title}
        src={src}
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      />
    </Container>
  );
}
