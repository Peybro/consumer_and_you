import React from "react";

import { Link } from "gatsby";
import styled from "styled-components";

const BackBtn = styled(Link)`
  font-size: 2rem;
  text-decoration: none;
  color: yellow;
  background: rgb(163, 163, 163);
  padding: 5px 10px;
`;

export default function Back({ to }) {
  return <BackBtn to={to}>{"<-back"}</BackBtn>;
}
