import React from "react";

import styled from "styled-components";

const ProductCard = styled.a`
  display: flex;
  max-width: 400px;
  max-height: 400px;
  overflow: hidden;
  position: relative;
  justify-content: center;
  align-items: center;
  background: #fff;

  // @media screen and (max-width: 320px) {
  //   width: 200px;
  //   height: 200px;
  // }
`;

const ProductImg = styled.img`
  width: 100%;
`;

export default function Product({ title, imgSrc, imgAlt, link }) {
  return (
    // eslint-disable-next-line react/jsx-no-target-blank
    <ProductCard href={link} target="_blank">
      <ProductImg src={imgSrc} alt={imgAlt} />
    </ProductCard>
  );
}
