import * as React from "react";
import { Link } from "gatsby";
import { Helmet } from "react-helmet";
import styled, { keyframes } from "styled-components";

import GlobalLayout from "./../layouts/GlobalLayout";
import Consumer from "../components/Consumer";

//styles
const rainbow = keyframes`
  0% {
    color: pink;
  }
  25% {
    color: lightgreen;
  }
  75% {
    color: cyan;
  }
  100% {
    color: pink;
  }
`;

const StartPage = styled.div`
  margin: 0;
  padding: 0;

  height: 100vh;
  overflow: hidden;
`;

const Background = styled.div`
  margin-top: -3%;
  margin-left: -7%;
  width: 140vw;
  color: #fff;
  animation: ${rainbow} 1s infinite;
  background: #000;
  font-size: 4rem;
  position: fixed;
  filter: blur(2px);
  word-break: break-all;
`;

const HeaderContainer = styled.div`
  display: flex;
  height: 100vh;
  justify-content: center;
  align-items: center;
`;

const Header = styled(Link)`
  color: yellow;
  font-size: 2rem;
  z-index: 10;
  text-decoration: none;
  text-align: center;
`;

export default function Start() {
  return (
    <GlobalLayout>
      <StartPage>
        <Helmet>
          <title>Startpage</title>
          <meta name="description" content="Welcome on Consumer_And_You." />
        </Helmet>
        <Background>
          <Consumer amount={300} />
        </Background>
        <HeaderContainer>
          <Header to="/intro">
            <h1>Consumer</h1>
            <h2>/kənˈsjuːmə(r)/</h2>
          </Header>
        </HeaderContainer>
      </StartPage>
    </GlobalLayout>
  );
}
