import React from "react";
import styled from "styled-components";

import Layout from "../layouts/Layout";
import CategoryCard from "./../components/IntroCard";

const CardContainer = styled.div`
  display: grid;
  grid-column-gap: 50px;
  row-gap: 50px;

  @media screen and (max-width: 320px) {
    grid-template-columns: auto;
    row-gap: 20px;
  }

  @media screen and (min-width: 720px) {
    grid-template-columns: auto auto;
  }
`;

// markup
const Intro = () => {
  return (
    <Layout>
      <CardContainer>
        <CategoryCard
          title={"Shopping"}
          to={"/shop"}
          img={{
            alt: "cart",
            src:
              "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fdijf55il5e0d1.cloudfront.net%2Fimages%2Fna%2F3%2F7%2F4%2F37497_1000.jpg&f=1&nofb=1",
          }}
        />
        <CategoryCard
          title={"Cinema"}
          to={"/cinema"}
          img={{
            alt: "cinema",
            src:
              "https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fc.pxhere.com%2Fphotos%2Ff5%2Fbe%2Fcolumbus_ohio_ohio_theatre_theater_marquee_front_entrance_downtown-1206124.jpg!d&f=1&nofb=1",
          }}
        />
      </CardContainer>
    </Layout>
  );
};

export default Intro;
