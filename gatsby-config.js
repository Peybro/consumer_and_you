module.exports = {
  siteMetadata: {
    title: "consumer_and_you",
    description: "test",
    author: "Hansi",
    siteUrl: "https://consumer-gatsby.netlify.com",
  },
  plugins: [
    "gatsby-plugin-styled-components",
    "gatsby-plugin-react-helmet",
    "gatsby-plugin-sitemap",
    {
      resolve: "gatsby-plugin-manifest",
      options: {
        icon: "src/images/icon.png",
      },
    },
  ],
};
